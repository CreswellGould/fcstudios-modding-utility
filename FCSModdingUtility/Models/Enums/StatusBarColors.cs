﻿namespace FCSModdingUtility
{
    /// <summary>
    /// The colors for the status bar
    /// </summary>
    public enum StatusBarColors
    {
        Default,
        Warning,
        Error,
        Unknown
    }
}
