﻿namespace FCSModdingUtility
{
    /// <summary>
    /// A page of the application
    /// </summary>
    public enum ApplicationPage
    {
        /// <summary>
        /// Initial load profile page
        /// </summary>
        StartPage = 0,

        /// <summary>
        /// The settings page
        /// </summary>
        Settings = 2,

        /// <summary>
        /// The new project page
        /// </summary>
        NewProject = 3,

        /// <summary>
        /// The editor page
        /// </summary>
        EditorPage = 4
    }
}
