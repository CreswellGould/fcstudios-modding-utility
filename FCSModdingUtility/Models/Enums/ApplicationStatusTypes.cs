﻿namespace FCSModdingUtility
{
    public enum ApplicationStatusTypes
    {
        Ready,
        Saving,
        Loading,
        Error,
        Running,
        Warning
    }
}
