﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCSModdingUtility
{
    public enum IssueReportType
    {
        VersionMissMatch,
        MissingDependancy,
        ModDisabled,
        Unknown
    }
}
